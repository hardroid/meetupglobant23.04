package cl.salemlabs.animelist;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Set;

import cl.salemlabs.animelist.adapters.SeriesRecyclerAdapter;
import cl.salemlabs.animelist.api.ApiCallBack;
import cl.salemlabs.animelist.api.ApiConnection;
import cl.salemlabs.animelist.interfaces.ApiEndPointInterface;
import cl.salemlabs.animelist.models.Series;
import cl.salemlabs.animelist.models.TransitionsView;
import cl.salemlabs.animelist.presenter.ListadoPresenter;
import cl.salemlabs.animelist.presenter.interfaces.ListadoActivityItemView;
import cl.salemlabs.animelist.presenter.interfaces.ListadoActivityViews;
import cl.salemlabs.animelist.viewholders.SerieViewHolders;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;

public class ListadoActivity extends BaseActivity implements ListadoActivityViews, ListadoActivityItemView {

    private ListadoPresenter presenter;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter<RecyclerView.ViewHolder> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        presenter = new ListadoPresenter(this, this);
        presenter.loadSeries();
        mRecyclerView = (RecyclerView)findViewById(R.id.rv_listado);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    public void seleccionarMenu() {

    }

    @Override
    public void salirActividad() {

    }

    @Override
    public void onErrorMessage(String title, String message) {

    }

    @Override
    public void onSucces(Series[] series) {
        loadRecyclerView(series);
    }

    private void loadRecyclerView(Series[] series) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new SeriesRecyclerAdapter(this, series, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void seleccionarSerie(TransitionsView view, Series serie) {
        Intent i = new Intent(this, DetailsActivity.class);
        i.putExtra(DetailsActivity.TITLE, serie.getTitle());
        i.putExtra(DetailsActivity.YEAR, serie.getYear());
        i.putExtra(DetailsActivity.URL_IMAGEN, serie.getPoster().getBanner());

        /*startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);*/

        View decor = getWindow().getDecorView();
        View statusBar = decor.findViewById(android.R.id.statusBarBackground);
        View navBar = decor.findViewById(android.R.id.navigationBarBackground);
        View actionBar = getActionBar(getWindow().getDecorView());
        ActivityOptionsCompat options = null;
        if(navBar != null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    // the context of the activity
                    this,
                    new Pair<View, String>(statusBar, statusBar.getTransitionName()),

                    new Pair<View, String>(navBar, navBar.getTransitionName()),
                    new Pair<View, String>(view.getImageView(), getString(R.string.transition_image)),
                    new Pair<View, String>(view.getTvTitle(), getString(R.string.transition_title)),
                    new Pair<View, String>(actionBar, actionBar.getTransitionName()),
                    new Pair<View, String>(view.getTvYear(), getString(R.string.transition_year))
            );
        }else{
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    // the context of the activity
                    this,
                    new Pair<View, String>(statusBar, statusBar.getTransitionName()),
                    new Pair<View, String>(actionBar, actionBar.getTransitionName()),
                    new Pair<View, String>(view.getImageView(), getString(R.string.transition_image)),
                    new Pair<View, String>(view.getTvTitle(), getString(R.string.transition_title)),
                    new Pair<View, String>(view.getTvYear(), getString(R.string.transition_year))
            );
        }

        ActivityCompat.startActivity(this, i, options.toBundle());
    }

    public ViewGroup getActionBar(View view) {
        try {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;

                if (viewGroup instanceof Toolbar) {
                    return viewGroup;
                }

                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    ViewGroup actionBar = getActionBar(viewGroup.getChildAt(i));

                    if (actionBar != null) {
                        return actionBar;
                    }
                }
            }
        } catch (Exception e) {
        }

        return null;
    }

}
