package cl.salemlabs.animelist.viewholders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cl.salemlabs.animelist.R;

/**
 * Created by hardroidlabs on 19-05-17.
 */

public class SerieViewHolders extends RecyclerView.ViewHolder {

    public CardView cardView;
    public ImageView imageView;
    public TextView tvtitle, tvYear, tvGenres, tvSesonsNumber, tvRating;

    public SerieViewHolders(View itemView) {
        super(itemView);
        cardView = (CardView)itemView.findViewById(R.id.cardView);
        imageView = (ImageView)itemView.findViewById(R.id.imageView);
        tvtitle =(TextView) itemView.findViewById(R.id.tv_title);
        tvYear=(TextView) itemView.findViewById(R.id.tv_year);
        tvGenres=(TextView) itemView.findViewById(R.id.tv_genres);
        tvSesonsNumber=(TextView) itemView.findViewById(R.id.tv_seasons_number);
        tvRating=(TextView) itemView.findViewById(R.id.tv_rating);
    }
}
