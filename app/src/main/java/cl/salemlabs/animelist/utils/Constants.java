package cl.salemlabs.animelist.utils;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public class Constants {
    static public final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    static public final String RELEASE = "release";
    static public final int API_CONNECTION_TIMEOUT = 10;
    public interface APIConstants {
        String ANIME_LIST = "animelist";
        String ANIME_ID = "anime/{id}";
    }
}
