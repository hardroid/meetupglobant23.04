package cl.salemlabs.animelist.utils;

import java.io.IOException;



public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "No Internet access";
    }
}