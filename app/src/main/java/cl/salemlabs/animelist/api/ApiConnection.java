package cl.salemlabs.animelist.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import cl.salemlabs.animelist.BuildConfig;
import cl.salemlabs.animelist.interfaces.ApiEndPointInterface;
import cl.salemlabs.animelist.utils.Constants;
import cl.salemlabs.animelist.utils.NetworkUtils;
import cl.salemlabs.animelist.utils.NoConnectivityException;
import ir.mirrajabi.okhttpjsonmock.interceptors.OkHttpMockInterceptor;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public class ApiConnection {
    private static ApiEndPointInterface apiEndPointInterface;
    private static Context mContext;
    private static Retrofit retrofit;

    public static ApiEndPointInterface getApi(Context context) {
        mContext = context;
        if (apiEndPointInterface == null)
            initialize();

        return apiEndPointInterface;
    }



    public static void initialize() {
        Gson gson = new GsonBuilder()
                .setDateFormat(Constants.ISO_DATE_FORMAT)
                .create();

        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.IS_DEVELOPMENT)  {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC));
            builder.addInterceptor(new OkHttpMockInterceptor(mContext, 0));
        }

        builder.readTimeout(Constants.API_CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        builder.connectTimeout(Constants.API_CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        final OkHttpClient okHttpClient = builder.build();



        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        apiEndPointInterface = retrofit.create(ApiEndPointInterface.class);

    }
}
