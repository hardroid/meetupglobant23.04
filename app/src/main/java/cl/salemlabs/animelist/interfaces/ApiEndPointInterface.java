package cl.salemlabs.animelist.interfaces;

import cl.salemlabs.animelist.models.Series;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static cl.salemlabs.animelist.utils.Constants.APIConstants.ANIME_ID;
import static cl.salemlabs.animelist.utils.Constants.APIConstants.ANIME_LIST;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public interface ApiEndPointInterface {
    @GET(ANIME_LIST)
    Call<Series[]> getListAnime();

    @GET(ANIME_ID)
    Call<Series[]> getDetailsAnime(@Path("id") String id);

}
