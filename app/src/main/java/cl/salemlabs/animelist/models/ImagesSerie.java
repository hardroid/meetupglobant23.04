package cl.salemlabs.animelist.models;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public class ImagesSerie {
    private String poster;
    private String fanart;
    private String banner;

    public ImagesSerie() {
    }

    public ImagesSerie(String poster, String fanart, String banner) {
        this.poster = poster;
        this.fanart = fanart;
        this.banner = banner;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getFanart() {
        return fanart;
    }

    public void setFanart(String fanart) {
        this.fanart = fanart;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
