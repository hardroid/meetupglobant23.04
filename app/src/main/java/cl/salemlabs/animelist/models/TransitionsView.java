package cl.salemlabs.animelist.models;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by hardroidlabs on 19-05-17.
 */

public class TransitionsView {

    private ImageView imageView;
    private TextView tvTitle;
    private TextView tvYear;

    public TransitionsView(ImageView imageView, TextView tvTitle, TextView tvYear) {
        this.imageView = imageView;
        this.tvTitle = tvTitle;
        this.tvYear = tvYear;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TextView getTvTitle() {
        return tvTitle;
    }

    public void setTvTitle(TextView tvTitle) {
        this.tvTitle = tvTitle;
    }

    public TextView getTvYear() {
        return tvYear;
    }

    public void setTvYear(TextView tvYear) {
        this.tvYear = tvYear;
    }
}
