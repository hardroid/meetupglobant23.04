package cl.salemlabs.animelist.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import cl.salemlabs.animelist.R;
import cl.salemlabs.animelist.models.Series;
import cl.salemlabs.animelist.models.TransitionsView;
import cl.salemlabs.animelist.presenter.interfaces.ListadoActivityItemView;
import cl.salemlabs.animelist.viewholders.SerieViewHolders;

/**
 * Created by hardroidlabs on 19-05-17.
 */

public class SeriesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ListadoActivityItemView listadoActivityItemView;
    private Context context;
    private Series[] dataSource;

    public SeriesRecyclerAdapter(Context context, Series[] dataSource, ListadoActivityItemView listadoActivityItemView) {
        this.context = context;
        this.dataSource = dataSource;
        this.listadoActivityItemView = listadoActivityItemView;
    }

    @Override
    public SerieViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.series_item, parent, false);
        SerieViewHolders serieViewHolders = new SerieViewHolders(v);
        return serieViewHolders;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderParam, int position) {
        final SerieViewHolders holder = (SerieViewHolders)holderParam;
        final Series series = dataSource[position];
        holder.tvtitle.setText(series.getTitle());
        holder.tvYear.setText(series.getYear());
        if(series.getRatingSerie() != null) {
            try {
                holder.tvRating.setText(series.getRatingSerie().getPercentage() + "%");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        int legthGenres = series.getGenres().length;
        holder.tvGenres.setText("[ ");
        for(int x = 0; x < legthGenres-1; x++){
         holder.tvGenres.append(series.getGenres()[x] + ", ");
        }

        holder.tvGenres.append(series.getGenres()[legthGenres-1] + " ]");
        holder.tvSesonsNumber.setText(series.getNumSeasons()+"");
        if(series.getPoster() != null){
            Picasso.with(context).load(series.getPoster().getBanner()).placeholder(R.mipmap.ic_launcher).into(holder.imageView);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listadoActivityItemView != null) {
                    listadoActivityItemView.seleccionarSerie(new TransitionsView(holder.imageView,holder.tvtitle, holder.tvYear), series);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSource.length;
    }
}
