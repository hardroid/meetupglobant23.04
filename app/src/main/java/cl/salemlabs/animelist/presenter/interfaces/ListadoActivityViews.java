package cl.salemlabs.animelist.presenter.interfaces;

import cl.salemlabs.animelist.models.Series;

/**
 * Created by hardroidlabs on 19-05-17.
 */

public interface ListadoActivityViews {

    void seleccionarMenu();
    void salirActividad();
    void onErrorMessage(String title, String message);
    void onSucces(Series[] series);
}
