package cl.salemlabs.animelist.presenter;

import android.content.Context;
import android.widget.Toast;

import cl.salemlabs.animelist.ListadoActivity;
import cl.salemlabs.animelist.api.ApiCallBack;
import cl.salemlabs.animelist.api.ApiConnection;
import cl.salemlabs.animelist.interfaces.ApiEndPointInterface;
import cl.salemlabs.animelist.models.Series;
import cl.salemlabs.animelist.presenter.interfaces.ListadoActivityViews;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by hardroidlabs on 19-05-17.
 */

public class ListadoPresenter {
    private ListadoActivityViews view;
    private Context context;
    private ApiCallBack<Series[]> callBack;

    public ListadoPresenter(ListadoActivityViews view, Context context) {
        this.view = view;
        this.context = context;
        setupCallBack();
    }

    private void setupCallBack() {
        callBack = new ApiCallBack<Series[]>() {
            @Override
            public void onInternetConnectionError() {
                view.onErrorMessage("Error de Conexión", "Ups error!!! onInternetConnectionError");
            }

            @Override
            public void onFailured(Throwable t) {
                view.onErrorMessage("Error de Conexión", "Ups error!!! onFailured");
            }

            @Override
            public void onRequestSuccess(Response<Series[]> response, int statusCode) {
                view.onSucces(response.body());
            }

            @Override
            public void onRequestError(Response<Series[]> response, int statusCode) {
                view.onErrorMessage("Error de Conexión", "Ups error!!! onFailured");
            }
        };
    }

    public void loadSeries(){
        //TODO acá progres loading
        final ApiEndPointInterface service = ApiConnection.getApi(context);
        Call<Series[]> loginResponse = service.getListAnime();

        loginResponse.enqueue(callBack);
    }


}
