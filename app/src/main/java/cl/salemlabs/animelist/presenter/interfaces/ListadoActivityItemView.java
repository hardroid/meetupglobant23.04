package cl.salemlabs.animelist.presenter.interfaces;

import cl.salemlabs.animelist.models.Series;
import cl.salemlabs.animelist.models.TransitionsView;

/**
 * Created by hardroidlabs on 19-05-17.
 */

public interface ListadoActivityItemView {
    void seleccionarSerie(TransitionsView viewHolders, Series serie);
}
