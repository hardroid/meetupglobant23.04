package cl.salemlabs.animelist;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    public static final String TITLE = "title";
    public static final String YEAR = "year";
    public static final String URL_IMAGEN = "url_imagen";

    private ImageView imageView;
    private TextView tvTitle, tvYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        imageView = (ImageView)findViewById(R.id.imageView);
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvYear = (TextView)findViewById(R.id.tv_year);
        String urlImage = getIntent().getStringExtra(URL_IMAGEN);
        if(urlImage != null){
            Picasso.with(this).load(urlImage).placeholder(R.mipmap.ic_launcher).into(imageView);
        }
        tvTitle.setText(getIntent().getStringExtra(TITLE));
        tvYear.setText(getIntent().getStringExtra(YEAR));

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void onBackPressed(){
        //you can do your other onBackPressed logic here..

        //Then just call finish()
        super.onBackPressed();/*
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);*/
    }



}
